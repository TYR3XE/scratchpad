package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

public class BookTable {

	private JFrame frame;
	private JTextField textField;
	private JScrollPane scrollPane;
	private JTable table;
	DefaultTableModel dTableModel;
	JPanel jp;
	String[] columnNames = {"Name", "FachBereichesLeiter","Abteilung",
	    		"Autor","BuchTitle","Auflag","Jahrgang","Verlag","IsbnNR","Medium",
	    		"AnzahlExamplare","EinzelPreis","GesamtPreis"};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookTable window = new BookTable();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BookTable() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Book Library");
		frame.setBounds(100, 100, 747, 519);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(6, 7, 118, 27);
		frame.getContentPane().add(comboBox);
		
		textField = new JTextField();
		textField.setBounds(158, 6, 354, 26);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton searchBtn = new JButton("New button");
		searchBtn.setBounds(577, 6, 117, 29);
		frame.getContentPane().add(searchBtn);
		createTable();
		jp= new JPanel(new BorderLayout());
		jp.add(table, BorderLayout.CENTER);
		
		scrollPane = new JScrollPane(jp);
		scrollPane.setBounds(16, 46, 708, 430);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
	}
	
	private void createTable() {
		DBConnection db = new DBConnection();
        String sql = "SELECT * FROM book";
        ArrayList<Book> b = db.connectToRead(sql);
        Object [][] daten = db.bookConverter(b);
        dTableModel = new DefaultTableModel(daten, columnNames);
        table = new JTable(dTableModel);

		
	}

}
