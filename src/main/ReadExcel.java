package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import sun.jvm.hotspot.ui.EditableAtEndDocument;

public class ReadExcel {

    public Object[][] read() throws IOException {
        File excelFile = new File("../scratchpad/rsc/SchülerversionBücherbestellung 2019_20 (1)(1-75).xlsx");
        FileInputStream fis = new FileInputStream(excelFile);

        // we create an XSSF Workbook object for our XLSX Excel File
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        // we get first sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        //the last not empty row number in the sheet
        int lastRow = sheet.getLastRowNum();
        //the first row in the sheet
        XSSFRow firstRow = sheet.getRow(1);
        //the last not empty row number in the sheet
        int lastColumn = firstRow.getLastCellNum();
        // create  Object array of 2 index to save the excel info
        Object [][] daten = new Object[lastRow-1][lastColumn];
        Object [][] filteredDaten = datenFilter(daten,lastColumn,lastRow, workbook);

        return filteredDaten ;
    }
    public Object[][] datenFilter(Object[][] daten, int lastColumn, int lastRow, XSSFWorkbook workbook){
        for(int x=1; x<lastRow; x++){
            for(int y=0; y<lastColumn; y++){
                XSSFCell cell =  workbook.getSheetAt(0).getRow(x).getCell(y);
                String cellAsString = cell.toString();
                switch (y){
                    //string
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                    case 7:
                    case 9:
                        if (cellAsString.isEmpty()){
                            daten[x-1][y]= "0";
                            continue;
                        }

                        daten[x-1][y] = cellAsString;
                        break;
                    //int
                    case 2:
                    case 5:
                    case 6:
                        //in excel Table (jahrgang) is sometimes Empty
                        if (cellAsString.isEmpty()){
                            daten[x-1][y]= 0;
                            continue;
                        }
                        //contain non numeric characters
                        if(!cellAsString.contains("[a-zA-Z]")){
                            double s= Double.parseDouble(cellAsString);
                            daten[x-1][y] = (int) s;
                        }else {
                            //remove all non numeric characters
                            String cellAsNum = cellAsString.replaceAll("[^\\d.]", "");
                            //remove all spaces
                            cellAsNum = cellAsNum.replaceAll(" ", "");
                            daten[x-1][y] = Integer.parseInt(cellAsNum);;
                        }
                        break;
                    //ISBN number contains (-)
                    case 8:
                    	//all characters except (-)
                        String cellAsISBN = cellAsString.replaceAll("[^0-9!@\\-]", "");
                        daten[x-1][y] = cellAsISBN;
                        break;
                        //double to integer
                    case 10 :
                        double s= Double.parseDouble(cellAsString);
                        daten[x-1][y] = (int) s;

                        break;
                     //double 25.2
                    case 11:
                        // if number 25,3
                        if(cellAsString.contains(",")){
                            cellAsString = cellAsString.replace("," , ".");
                        }
                        double cellAsDouble = Double.parseDouble(cellAsString);
                        daten[x-1][y] = cellAsDouble;
                        break;
                    case 12 :
                        int anzahl =(int) daten[x-1][y-2];
                        double prise = (double)daten[x-1][y-1];
                        daten[x-1][y] = anzahl*(prise);

                        break;
                    default:
                        daten[x-1][y] = 0;
                }
            }
        }
        return daten;
    }
}