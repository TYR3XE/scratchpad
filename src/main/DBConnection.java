package main;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnection {
 
    public static void connectToWrite(Object data[][]) {
        try {
            String url = "jdbc:sqlite:../scratchpad/rsc/booklibrary.db";
            Connection conn = DriverManager.getConnection(url,"","");
            Statement st = conn.createStatement();
            insertInDB(data,st);
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

    }
    
    public static void insertInDB (Object data[][], Statement st) throws SQLException {
        String values = "";
        for (int i=0; i<data.length; i++){
            values = "VALUES ( ";
            for (int y=0 ; y<data[i].length; y++){
                //String
                if (data[i][y].getClass().getName() == Type.STRING) {
                    if (y<data[i].length-1){
                        values = values+"'"+data[i][y]+"' , ";
                    }else {
                        values = values+"'"+data[i][y]+"' ";
                    }
                    //int
                }  else if (data[i][y] instanceof Integer) {
                    if (y<data[i].length-1){
                        values = values+data[i][y]+" , ";
                    }else {
                        values = values+data[i][y];
                    }
                }else if(data[i][y] instanceof Double){
                    if (y<data[i].length-1){
                        values = values+data[i][y]+" , ";
                    }else {
                        values = values+data[i][y];
                    }
                }
            }
            // Example for values :  "VALUES ('name1' , 'bla bla' , 2, 'c','V',2, 23, 23, 'd', 2, 2.3)"
            values = values+ " )";
            try {
                st.executeUpdate("INSERT INTO book " + values);

            }catch (Exception e){
                System.err.println(e.getMessage());
            }
        }
    }
    public ArrayList<Book> connectToRead(String sql) {
    	ArrayList<Book> books = new ArrayList<Book>();
        try {
            String url = "jdbc:sqlite:../../LibraryManagmentlocal/booklibrary.db";
            Connection conn = DriverManager.getConnection(url,"","");
            Statement st = conn.createStatement();
            books =readFromDB(sql ,st);
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
		return books;

    }
    public static ArrayList<Book> readFromDB ( String sql ,Statement stmt) throws SQLException{
        ArrayList<Book> books = new ArrayList<Book>();
    	 try (ResultSet result = stmt.executeQuery(sql)){
    		 Object re = result ;
    		 System.out.print("fertig");
                //int i = 0;
                 //loop through the result set
                
                	while (result.next()) {
                		int anzahl =result.getInt("anzahlExamplare");
                		  for (int x= 0;x<anzahl ;x++) {
                			  Book book =new Book();
                      		
                              book.setName(result.getString("name"));
                              book.setFachBereichesLeiter(result.getString("fachBereichesLeiter"));
                              book.setAbteilung(result.getInt("abteilung"));
                              book.setAutor(result.getString("autor"));
                              book.setBuchTitle(result.getString("buchTitle"));
                              book.setAuflag(result.getInt("auflag"));
                              book.setJahrgang(result.getInt("jahrgang"));
                              book.setVerlag(result.getString("verlag"));
                              book.setIsbnNR(result.getString("isbn_nr"));
                              book.setMedium(result.getString("medium"));
                              book.setBookNr(x);
                              book.setEinzelPreis(result.getDouble("einzelPreis"));
                             //book.setGesamtPreis(result.getDouble("gesamtPreis"));
                      		//i = i+1;
                      		books.add(book);
                			  
                		  }
                			
                    }
                System.out.print("fertig");
                
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
		
		return books;
        	
    }
    public static Object[][] bookConverter(ArrayList<Book> books){
    	Object [][] booksAsArray = new Object [books.size()][13]  ;
    	int x = 0;
    		for (Book book :books) {
    			booksAsArray[x][0] = book.getName();
    			booksAsArray[x][1] = book.getFachBereichesLeiter();
    			booksAsArray[x][2] = book.getAbteilung();    			
    			booksAsArray[x][3] = book.getAutor();
    			booksAsArray[x][4] = book.getBuchTitle();
    			booksAsArray[x][5] = book.getAuflag();
    			booksAsArray[x][6] = book.getJahrgang();
    			booksAsArray[x][7] = book.getVerlag();
    			booksAsArray[x][8] = book.getIsbnNR();
    			booksAsArray[x][9] = book.getMedium();
    			booksAsArray[x][10] = book.getBookNr();
    			booksAsArray[x][11] = book.getEinzelPreis();
    			//booksAsArray[x][12] = book.getGesamtPreis();
    			x =x+1;
        		
        	}
    	
		return booksAsArray;
    	
    }
}  