package main;

import java.util.ArrayList;

public class Book {
	String name ;
	String fachBereichesLeiter ;
	int abteilung; 
	String autor ; 
	String buchTitle ;
	int auflag ;
	int jahrgang ;
	String verlag;
	String isbnNR;
	String medium ;
	int bookNR ;
	double einzelPreis;
	//double gesamtPreis;
	public Book() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFachBereichesLeiter() {
		return fachBereichesLeiter;
	}
	public void setFachBereichesLeiter(String fachBereichesLeiter) {
		this.fachBereichesLeiter = fachBereichesLeiter;
	}
	public int getAbteilung() {
		return abteilung;
	}
	public void setAbteilung(int abteilung) {
		this.abteilung = abteilung;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getBuchTitle() {
		return buchTitle;
	}
	public void setBuchTitle(String buchTitle) {
		this.buchTitle = buchTitle;
	}
	public int getAuflag() {
		return auflag;
	}
	public void setAuflag(int auflag) {
		this.auflag = auflag;
	}
	public int getJahrgang() {
		return jahrgang;
	}
	public void setJahrgang(int jahrgang) {
		this.jahrgang = jahrgang;
	}
	public String getVerlag() {
		return verlag;
	}
	public void setVerlag(String verlag) {
		this.verlag = verlag;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public int getBookNr() {
		return bookNR;
	}
	public void setBookNr(int BookNr) {
		this.bookNR = BookNr;
	}
	public double getEinzelPreis() {
		return einzelPreis;
	}
	public void setEinzelPreis(double einzelPreis) {
		this.einzelPreis = einzelPreis;
	}
//	public double getGesamtPreis() {
//		return gesamtPreis;
//	}
//	public void setGesamtPreis(double gesamtPreis) {
//		this.gesamtPreis = gesamtPreis;
//	}
	public String getIsbnNR() {
		return isbnNR;
	}
	public void setIsbnNR(String isbnNR) {
		this.isbnNR = isbnNR;
	}
	
	
}
