package main;

import java.io.IOException;

public class main {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Hello World");
		ReadExcel reader = new ReadExcel() ;
        // get excel Table as Object [][]
        Object[][] daten =  reader.read();
        //data base connection
        DBConnection db = new DBConnection();
        db.connectToWrite(daten);
        String sql = "SELECT * FROM book";
        db.connectToRead(sql);

	}

}
